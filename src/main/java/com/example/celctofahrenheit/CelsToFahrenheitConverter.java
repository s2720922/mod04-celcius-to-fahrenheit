package com.example.celctofahrenheit;


public class CelsToFahrenheitConverter {
    //F = C * 1.8 + 32
    //C = (F - 32) / 1.8
    public static double convertToFahrenheit(double celcius, int decimalValue) {
        if (decimalValue < 0) {
            throw new IllegalArgumentException("DecimalValue negative");
        }
        return Math.floor((celcius * 1.8 + 32) * Math.pow(10, decimalValue)) / Math.pow(10, decimalValue);
    }
}
