package com.example.celctofahrenheit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class CelciusToFahrenheit extends HttpServlet {
    @Override
    public void init() throws ServletException {
        //do something
    }


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //do response stuff and convert Celsius to fahrenheit
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        Double celsius = null;
        try {
            celsius = Double.parseDouble(request.getParameter("celsius"));
        } catch (NumberFormatException e) {
            //do nothing
        }

        String page = HTMLGenerator.generateHTML("Celsius to Fahrenheit",
                "<h1>" + "Celcius to Fahrenheit conversion" + "</h1>\n" +
                "  <p>Celsius: \t" + request.getParameter("celsius") + //"</P>\n" +
                "  <p>Fahrenheit: \t" + (celsius == null ? "Invalid celsius number"
                        : CelsToFahrenheitConverter.convertToFahrenheit(celsius, 6)) +
                "\n<form method=\"get\" action=\"./index.html\">" +
                        "\n<input type=\"submit\" value=\"Go back to converting\">\n" +
                        "</form>"); //+ "</P>";
        out.println(page);
    }
}
