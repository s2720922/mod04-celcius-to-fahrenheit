package com.example.celctofahrenheit;

public class HTMLGenerator {
    public static String generateHTML(String title, String content) {
        return "<!DOCTYPE HTML>" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                content +
                "\n</BODY></HTML>";
    }
}
